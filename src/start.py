#!/usr/bin/env python3
# Run this script to start LyBerry dmenu without installing
from lyberry_dmenu.lyberrydmenu import main

main()
